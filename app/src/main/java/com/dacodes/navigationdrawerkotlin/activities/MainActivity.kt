package com.dacodes.navigationdrawerkotlin.activities

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.dacodes.navigationdrawerkotlin.Appcontroller
import com.dacodes.navigationdrawerkotlin.R
import com.dacodes.navigationdrawerkotlin.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), com.dacodes.navigationdrawerkotlin.adapters.MenuAdapter.CustomMenuClickListener {


    lateinit var menuAdapter : com.dacodes.navigationdrawerkotlin.adapters.MenuAdapter
    var appcontroller: Appcontroller = Appcontroller()

    lateinit var actionBarDrawerToggle : ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportFragmentManager
                .beginTransaction()
                .add(R.id.flContainer, HomeFragment.newInstance("arg1", "arg2"), HomeFragment.javaClass.name)
                .commit()

        setupSlideMenu()


        /**
         * OPEN THE SIDE MENU WITH TOOLBAR ICON
         */
        ivMenu.setOnClickListener {
            drawerlayout.openDrawer(rvScrollMenu)
        }


    }


    /**
     * INITIALIZE AND CONFIGURE THE SIDE MENU
     */

    private fun setupSlideMenu() {

        rvScrollMenu.layoutManager = LinearLayoutManager(this)
        menuAdapter = com.dacodes.navigationdrawerkotlin.adapters.MenuAdapter(this, this)
        rvScrollMenu.adapter = menuAdapter

        menuAdapter.addHeader(appcontroller)
        menuAdapter.addTextItem(R.drawable.ic_home, getString(R.string.nav_drawer_home), menuAdapter.OPTION_1)
        menuAdapter.addTextItem(R.drawable.ic_settings, getString(R.string.nav_drawer_settings), menuAdapter.OPTION_2)
        menuAdapter.addTextItem(R.drawable.ic_contact, getString(R.string.nav_drawer_contact), menuAdapter.OPTION_3)
        menuAdapter.addTextItem(R.drawable.ic_logout, getString(R.string.nav_drawer_logout), menuAdapter.LOG_OUT)

        menuAdapter.notifyDataSetChanged()


        drawerlayout.setScrimColor(Color.TRANSPARENT)
        drawerlayout.setBackgroundColor(Color.TRANSPARENT)
        drawerlayout.closeDrawer(rvScrollMenu)
        // Initialize the action bar drawer toggle instance
        actionBarDrawerToggle = object : ActionBarDrawerToggle(
                this,
                drawerlayout,
                null,
                R.string.drawer_open,
                R.string.drawer_close
        ){
            override fun onDrawerClosed(view: View){
                super.onDrawerClosed(view)
                invalidateOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View){
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu()

            }
        }

        drawerlayout.addDrawerListener(actionBarDrawerToggle)

    }

    /**
     * INTERFACE OPTIONS SIDE MENU
     */

    override fun onCustomMenuItemClick(position: Int, type: Int, id: Int) {

        drawerlayout.closeDrawer(rvScrollMenu)

        when(id){

            menuAdapter.SHOW_PROFILE -> {

                Log.v("ITEM", "HEADER")

            }

            menuAdapter.OPTION_1 -> {

                Log.v("ITEM", "OPTION_1")

            }

            menuAdapter.OPTION_2 -> {

                Log.v("ITEM", "OPTION_2")

            }

            menuAdapter.OPTION_3 -> {

                Log.v("ITEM", "OPTION_3")

            }

            menuAdapter.LOG_OUT -> {

                Log.v("ITEM", "LOG_OUT")

            }

        }

    }


}
