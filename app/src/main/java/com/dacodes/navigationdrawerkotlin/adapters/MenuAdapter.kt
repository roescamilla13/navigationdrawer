package com.dacodes.navigationdrawerkotlin.adapters

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.dacodes.navigationdrawerkotlin.Appcontroller
import com.dacodes.navigationdrawerkotlin.entities.ItemMenu
import com.dacodes.navigationdrawerkotlin.R


class MenuAdapter( val context : Context , val listener : CustomMenuClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = mutableListOf<ItemMenu>()
    val HEADER_ITEM = 0
    val SIMPLE_ROW_ITEM = 1

    val SHOW_PROFILE = 200
    val OPTION_1 = 201
    val OPTION_2 = 202
    val OPTION_3 = 203
    val LOG_OUT = 210

    interface CustomMenuClickListener {
        fun onCustomMenuItemClick(position: Int, type: Int, id: Int)
    }

    private inner class HearderItem internal constructor(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        internal var ivProfilePicture: ImageView
        internal var tvName: TextView
        internal var tvEmail: TextView

        init {
            ivProfilePicture = v.findViewById(R.id.ivProfilePicture)
            tvName = v.findViewById(R.id.tvName)
            tvEmail = v.findViewById(R.id.tvEmail)
            tvEmail.setOnClickListener(this)
            ivProfilePicture.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            listener.onCustomMenuItemClick(0, SHOW_PROFILE, SHOW_PROFILE)
            invalidateOptions()
        }

    }

    private inner class SimpleRowItem internal constructor(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        internal var ivIcon: ImageView
        internal var tvText: TextView

        init {
            v.setOnClickListener(this)
            ivIcon = v.findViewById(R.id.ivMenuItemIcon)
            tvText = v.findViewById(R.id.tvMenuItemText)
        }

        override fun onClick(v: View) {
            listener.onCustomMenuItemClick(adapterPosition, 0, items[adapterPosition].id)
            selectItem(adapterPosition)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        lateinit var view : View

        when(viewType){

            HEADER_ITEM -> {
                view = LayoutInflater.from(context)
                        .inflate(R.layout.menu_header,parent,false)

                return HearderItem(view)
            }

            SIMPLE_ROW_ITEM -> {
                view = LayoutInflater.from(context)
                        .inflate(R.layout.menu_row, parent, false)
                return SimpleRowItem(view)

            }

            else -> {
                view = LayoutInflater.from(context)
                        .inflate(R.layout.menu_row, parent, false)
                return SimpleRowItem(view)

            }

        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position : Int) {
        val viewType = viewHolder.getItemViewType()
        when (viewType) {
            HEADER_ITEM -> {

                val vhHI = viewHolder as HearderItem
                configureHeaderImage(vhHI, position)
            }

            SIMPLE_ROW_ITEM -> {

                val vhTI = viewHolder as SimpleRowItem
                configureTextItem(vhTI, position)
            }
        }


    }


    private fun configureHeaderImage(vh: HearderItem, position: Int) {
        if (!TextUtils.isEmpty(items[position].image)) {
            Glide.with(context).asBitmap()
                    .load(items[position].image)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(20)))
                    .into(vh.ivProfilePicture)

        } else {
            Glide.with(context).asBitmap()
                    .load(R.drawable.default_profile)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(20)))
                    .into(vh.ivProfilePicture)
        }
        vh.tvName.setText(items[position].textField1)
        vh.tvEmail.setText(items[position].textField2)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun configureTextItem(vh: SimpleRowItem, position: Int) {
        vh.ivIcon.setImageResource(items[position].cellIcon)
        vh.tvText.setText(items[position].cellTitle)

        if(items[position].selected){
            vh.tvText.setTextColor(context.getColor(R.color.colorAccent))
            vh.ivIcon.setColorFilter(context.getColor(R.color.colorAccent))
        }else{
            vh.tvText.setTextColor(context.getColor(R.color.colorWhite))
            vh.ivIcon.setColorFilter(context.getColor(R.color.colorWhite))
        }
    }


    override fun getItemCount(): Int {
        if(items!=null){
            return items.size
        }else{
            return 0
        }
    }


    override fun getItemViewType(position: Int): Int {
        return  items[position].typeOfCell
    }

    fun addHeader(app: Appcontroller) {
        val current = ItemMenu(HEADER_ITEM, app.userProfilePicture, app.userName, app.userEmail)
        items.add(current)
    }


    fun addTextItem(iconId: Int, textMessage: String, id: Int) {
        val current = ItemMenu(id, SIMPLE_ROW_ITEM, textMessage, iconId)
        items.add(current)
    }

    fun clearItems() {
        items.clear()
    }

    fun updateMenu(app: Appcontroller) {
        items[0].textField1 = app.userName
        items[0].textField2 = app.userEmail
        items[0].image = app.userProfilePicture
        notifyDataSetChanged()
    }

    fun selectItem(position : Int ){
        items[position].selected = true
        for ((index, value) in items.withIndex()){
            if (index != position){
                items[index].selected = false
            }
        }

        notifyDataSetChanged()
    }

    fun invalidateOptions(){
        for ((index, value) in items.withIndex()){
            items[index].selected = false
        }
        notifyDataSetChanged()
    }

}