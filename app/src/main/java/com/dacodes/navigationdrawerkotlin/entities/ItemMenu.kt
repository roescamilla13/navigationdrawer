package com.dacodes.navigationdrawerkotlin.entities

class ItemMenu{

    var id : Int = 0
    var typeOfCell : Int = 0
    var cellTitle : String  = ""
    var cellIcon : Int = 0
    var image : String = ""
    var bold : Boolean = false
    var textField1 : String = ""
    var textField2 : String = ""
    var selected : Boolean = false

    constructor()

    constructor(id : Int, typeOfCell : Int, cellTitle : String, cellIcon : Int){
        this.id = id
        this.typeOfCell = typeOfCell
        this.cellTitle = cellTitle
        this.cellIcon = cellIcon
    }

    constructor(typeOfCell: Int, image : String, textField1 : String, textField2: String){
        this.typeOfCell = typeOfCell
        this.image = image
        this.textField1 = textField1
        this.textField2 = textField2
    }

}





